class AlbumCommentsController < ApplicationController
  def create
		@album_comment = current_user.album_comments.build(album_comment_params)
		@album_comment.update_attributes(album_id: params[:album_id])
		@album_comment.save
		redirect_to users_show_path(Album.find_by(id: params[:album_id]).user_id)
	end

	def destroy
		@album_comment = AlbumComment.find(params[:id])
		@album_comment.destroy
		redirect_to users_show_path(Album.find_by(id: params[:album_id]).user_id)
	end

	private

	def album_comment_params
		params.require(:album_comment).permit(:body)
	end
end