class AlbumLikesController < ApplicationController
  def create
  	if AlbumLike.find_by(user_id: params[:current_user_id], album_id: params[:album_id])
  		AlbumLike.find_by(user_id: params[:current_user_id], album_id: params[:album_id]).destroy
  		redirect_to users_show_path(Album.find_by(id: params[:album_id]).user_id)
  	else
  		@album_like = AlbumLike.new(:album_id => params[:album_id], :user_id => params[:current_user_id])
  		@album_like.save
  		redirect_to users_show_path(Album.find_by(id: params[:album_id]).user_id)
  	end
  end
end
