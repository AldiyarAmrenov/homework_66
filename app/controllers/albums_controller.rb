class AlbumsController < ApplicationController
  def index
    @albums = Album.all
  end

  def show
    @album = Album.find(params[:id])
  end

  def new
    @album = Album.new
  end

  def create
    @album = current_user.albums.build(album_params)
    if @album.save
      upload_pictures
      redirect_to users_show_path(current_user)
    else
      render 'new'
    end
  end

  def edit
    @album = Album.find(params[:id])
  end

  def update
    @album = Album.find(params[:id])

    if @album.update(album_params)
      upload_pictures
      redirect_to root_path
    else
      render 'edit'
    end
  end

  def destroy
    @album = Album.find(params[:id])
    @album.destroy
    redirect_to root_path
  end

  private

  def album_params
    params.require(:album).permit(:title, :desc)
  end

  def upload_pictures
    @album.pictures.attach(uploaded_files) if uploaded_files.present?
  end

  def uploaded_files
    params[:album][:pictures]
  end

end
