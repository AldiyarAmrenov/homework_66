class FollowshipsController < ApplicationController
	def create
		@followship = Followship.new(:follower_user_id => params[:current_user_id], :user_to_follow_id => params[:user_id])
		@followship.save
		redirect_to users_show_path(params[:user_id])
	end

	def destroy
		@followship = Followship.find_by(follower_user_id: params[:current_user_id], user_to_follow_id: params[:user_id])
		@followship.destroy
		redirect_to users_show_path(params[:user_id])
	end
end
