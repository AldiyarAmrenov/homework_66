class PhotoCommentsController < ApplicationController
	def create
		@photo_comment = current_user.photo_comments.build(photo_comment_params)
		@photo_comment.update_attributes(photo_id: params[:photo_id])
		@photo_comment.save
		redirect_to users_show_path(Photo.find_by(id: params[:photo_id]).user_id)
	end

	def destroy
		@photo_comment = PhotoComment.find(params[:id])
		@photo_comment.destroy
		redirect_to users_show_path(Photo.find_by(id: params[:photo_id]).user_id)
	end

	private

	def photo_comment_params
		params.require(:photo_comment).permit(:body)
	end
end

