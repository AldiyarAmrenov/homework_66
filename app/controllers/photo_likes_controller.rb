class PhotoLikesController < ApplicationController
  def create
  	if PhotoLike.find_by(user_id: params[:current_user_id], photo_id: params[:photo_id])
  		PhotoLike.find_by(user_id: params[:current_user_id], photo_id: params[:photo_id]).destroy
  		redirect_to users_show_path(Photo.find_by(id: params[:photo_id]).user_id)
  	else
  		@photo_like = PhotoLike.new(:photo_id => params[:photo_id], :user_id => params[:current_user_id])
  		@photo_like.save
  		redirect_to users_show_path(Photo.find_by(id: params[:photo_id]).user_id)
  	end
  end
end
