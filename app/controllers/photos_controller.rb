class PhotosController < ApplicationController
  def index
    @photos = Photo.all
  end

  def show
    @photo = Photo.find(params[:id])
  end

  def new
     @photo = Photo.new
  end

  def create
    @photo = current_user.photos.build(photo_params)
    
    if @photo.save
      upload_image
      redirect_to users_show_path(current_user)
    else
      render 'new'
    end
  end

  def edit
    @photo = Photo.find(params[:id])
  end

  def update
    @photo = Photo.find(params[:id])

    if @photo.update(photo_params)
      upload_image
      redirect_to root_path
    else
      render 'edit'
    end
  end

  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy
    redirect_to root_path
  end

  private
  def photo_params
    params.require(:photo).permit(:title, :desc)
  end

  def path_to_the_file(new_file_path)
    string = new_file_path.to_s
    path = "/" + string.split("/")[string.split("/").index("upload")..-1].join("/")
    return path
  end

  def upload_image
    uploaded_file = params[:photo][:image]
    unless uploaded_file.nil?
      new_file_path = Rails.root.join('public', 'upload', 'photos', @photo.id.to_s)
      File.open(new_file_path, 'wb') do |file|
        file.write uploaded_file.read
      end
      @photo.update_attributes(image: path_to_the_file(new_file_path))
    end
  end
end