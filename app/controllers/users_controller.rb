class UsersController < ApplicationController
  def index
  	@users = User.all
  end

  def show
  	@user = User.find(params[:id])
  end

  def destroy
  end

  def followers_index
  	@followers = []
  	Followship.where(user_to_follow_id: params[:id]).each do |followship|
  		@followers << User.find(followship.follower_user_id)
  	end
  end

  def users_i_follow_index
  	@users_i_follow = []
  	Followship.where(follower_user_id: params[:id]).each do |followship|
  		@users_i_follow << User.find(followship.user_to_follow_id)
  	end
  end
end
