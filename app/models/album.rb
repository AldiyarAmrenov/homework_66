class Album < ApplicationRecord
	belongs_to :user
	has_many_attached :pictures
	has_many :album_comments
end
