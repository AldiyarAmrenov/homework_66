class Photo < ApplicationRecord
	belongs_to :user
	has_many :photo_likes
	has_many :photo_comments
end
