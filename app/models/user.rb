class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
	has_many :photos
	has_many :albums
	has_many :photo_likes
	has_many :album_likes
	has_many :photo_comments
	has_many :album_comments

	has_one_attached :avatar
end
