Rails.application.routes.draw do

  post 'photo_likes/:current_user_id/create/:photo_id' => 'photo_likes#create', as: 'create_photo_like'
  post 'album_likes/:current_user_id/create/:album_id' => 'album_likes#create', as: 'create_album_like'

  post 'followships/:current_user_id/create/:user_id' => 'followships#create', as: 'create_followship'
  delete 'followships/:current_user_id/destroy/:user_id' => 'followships#destroy', as: 'delete_followship'

  get 'users/:id/users_i_follow' =>'users#users_i_follow_index', as: 'users_i_follow'
  get 'users/:id/followers' => 'users#followers_index', as: 'followers'
  
  root 'users#index'
  devise_for :users
  get 'users/:id/show' => 'users#show', as: 'users_show'
  
  resources :photos do
    resources :photo_comments
  end

  resources :albums do
    resources :album_comments
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
