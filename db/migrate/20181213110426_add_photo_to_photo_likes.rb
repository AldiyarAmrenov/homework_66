class AddPhotoToPhotoLikes < ActiveRecord::Migration[5.2]
  def change
    add_reference :photo_likes, :photo, foreign_key: true
  end
end
