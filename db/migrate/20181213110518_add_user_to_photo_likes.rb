class AddUserToPhotoLikes < ActiveRecord::Migration[5.2]
  def change
    add_reference :photo_likes, :user, foreign_key: true
  end
end
