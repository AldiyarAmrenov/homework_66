class AddAlbumToAlbumLikes < ActiveRecord::Migration[5.2]
  def change
    add_reference :album_likes, :album, foreign_key: true
  end
end
