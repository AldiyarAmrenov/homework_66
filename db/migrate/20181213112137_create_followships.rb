class CreateFollowships < ActiveRecord::Migration[5.2]
  def change
    create_table :followships do |t|
      t.integer :follower_user_id
      t.integer :user_to_follow_id

      t.timestamps
    end
  end
end
