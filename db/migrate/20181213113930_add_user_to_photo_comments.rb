class AddUserToPhotoComments < ActiveRecord::Migration[5.2]
  def change
    add_reference :photo_comments, :user, foreign_key: true
  end
end
