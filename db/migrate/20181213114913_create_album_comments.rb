class CreateAlbumComments < ActiveRecord::Migration[5.2]
  def change
    create_table :album_comments do |t|
      t.text :body

      t.timestamps
    end
  end
end
