class AddUserToAlbumComments < ActiveRecord::Migration[5.2]
  def change
    add_reference :album_comments, :user, foreign_key: true
  end
end
