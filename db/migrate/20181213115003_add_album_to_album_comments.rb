class AddAlbumToAlbumComments < ActiveRecord::Migration[5.2]
  def change
    add_reference :album_comments, :album, foreign_key: true
  end
end
