require 'test_helper'

class AlbumCommentsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get album_comments_create_url
    assert_response :success
  end

  test "should get destroy" do
    get album_comments_destroy_url
    assert_response :success
  end

end
